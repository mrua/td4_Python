import requests
import csv
from bs4 import BeautifulSoup

#pour la partie csv
fichier = open('data.csv','w')
crayon = csv.writer(fichier)


UNI = requests.get("http://www.univ-orleans.fr")
soup = BeautifulSoup(UNI.text, "lxml")
mesdivs = soup.findAll("div", { "class" : "composite-zone" })
#print(soup.h1)
#print(soup.div)
#print(soup.img)
#print(soup.a)
#print(mesdivs)


#exo 3
UNI = requests.get("https://stackoverflow.com/questions/tagged/beautifulsoup")
soup = BeautifulSoup(UNI.text, "lxml")
questions =soup.findAll("a",{ "class" : "question-hyperlink" })
votes=soup.findAll("div",{ "class" : "votes" })
reponses=soup.findAll("div",{"class" : "status"} or {"class" : "status"})
listeVotes=[]
listeReponses=[]

for v in votes:
    listeVotes.append(v.strong.text)

for r in reponses:
    listeReponses.append(r.strong.text)


res=[]
i=0
for n in questions:
    if i<10:
        res.append((n.text,listeVotes[i],listeReponses[i]))
    i+=1

crayon.writerow(["Question","Vote","Reponse"])
for elem in res:
    crayon.writerow([elem[0],elem[1],elem[2]])
fichier.close()

with open('data.csv','r') as fichier:
    fichiercsv = csv.reader(fichier)
    for ligne in fichiercsv:
        print(ligne)


#exo4
print()
print()
data = {"submit-form":"","zone-item-id":"zoneItem://c8e50408-29b9-4eb9-9b3f-1c209fb2d75b","catalog":"catalogue-2015-2016","degree":"DP", "orgUnit":"orgunitContent://9ee7f4af-c6e8-406a-8292-dea5cdf178c1"}
r = requests.post("http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav", data = data)
soup = BeautifulSoup(r.text, "lxml")
mesres = soup.findAll("li", { "class" : "hit" })
for b in mesres:
    print(b.strong.text)


#exo5
print()
print()
def get_definition(x):
    URL ='http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'
    format(x)
    html = requests.get(URL).text
print()
print(get_definition("hello"))

def get_definition2(x):
    data = {"txtWord":x,"ddDictList":"*","btnSubmit":"search"}
    r = requests.post("http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}", data = data)
    soup = BeautifulSoup(r.text, "lxml")
    mesres = soup.findAll("pre")
    for c in mesres:
        print(c)
print()
print(get_definition2("hello"))

def get_definition3(x):
    url="http://services.aonaware.com/DictService/Default.aspx?action=define&dict=*&query="+x
    r=requests.get(url)
    soup = BeautifulSoup(r.text, "lxml")
    mesres = soup.findAll("pre")
    for d in mesres:
        print(d)
print()
print(get_definition3("hello"))
